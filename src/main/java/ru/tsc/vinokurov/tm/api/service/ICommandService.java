package ru.tsc.vinokurov.tm.api.service;

import ru.tsc.vinokurov.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
