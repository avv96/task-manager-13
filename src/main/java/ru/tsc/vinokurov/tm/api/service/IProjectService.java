package ru.tsc.vinokurov.tm.api.service;

import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    void clear();

}
